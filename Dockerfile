FROM mcr.microsoft.com/dotnet/sdk:6.0-bullseye-slim AS build
WORKDIR "/src"
RUN apt-get update && \
    apt-get install -y wget gnupg2 && \
    wget -qO- https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get install -y nodejs
COPY GitlabKubernetesBridge.sln .
COPY HelmChartUpdater HelmChartUpdater/
COPY GitlabKubernetesBridge GitlabKubernetesBridge/
RUN dotnet restore
RUN dotnet build -c Release -o /app
RUN dotnet publish -c Release -o /app


FROM debian:bullseye AS helm
WORKDIR /app
RUN apt-get update && apt-get install -y wget tar curl
RUN wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz
RUN tar -zxvf helm-v3.9.0-linux-amd64.tar.gz
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x kubectl

FROM mcr.microsoft.com/dotnet/aspnet:6.0-bullseye-slim AS final
WORKDIR /app
COPY --from=helm /app/linux-amd64/helm /usr/local/bin/
COPY --from=helm /app/kubectl /usr/local/bin/
ENV HELM_PATH=/usr/local/bin/helm
RUN /usr/local/bin/helm repo add gitlab https://charts.gitlab.io

COPY --from=build /app .
EXPOSE 80
ENTRYPOINT ["dotnet", "GitlabKubernetesBridge.dll"]
