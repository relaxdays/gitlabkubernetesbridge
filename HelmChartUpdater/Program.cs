﻿using HelmChartUpdater.models;
using k8s;
using System.Diagnostics;
using System.Text.Json;


// Check prerequisites
if (Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION") == null || Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION") == "")
{
    Console.WriteLine("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION environment variable not found - exiting");
    return;
}

Console.WriteLine("Updating helm local repository cache");
using (var p = new Process())
{
    p.StartInfo.FileName = Environment.GetEnvironmentVariable("HELM_PATH") ?? "/usr/local/bin/helm";
    p.StartInfo.ArgumentList.Add("repo");
    p.StartInfo.ArgumentList.Add("update");
    p.Start();
    p.WaitForExit();
    if (p.ExitCode != 0)
    {
        Console.WriteLine("ERROR: Failed to update Helm repositories. Exiting now. For errors see above.");
        Environment.Exit(1);
        return;
    }
}

Console.WriteLine("Begin updating Gitlab Kubernetes Agents in Cluster");

// Load from in-cluster configuration:
var config = KubernetesClientConfiguration.InClusterConfig();

// Use the config object to create a client.
var client = new Kubernetes(config);

var configuredNamespaces = client.ListNamespace(labelSelector: Org.OpenAPITools.Controllers.GitlabApiController.NamespaceProjectLabelName);

foreach (var ns in configuredNamespaces.Items)
{
    Console.WriteLine($"Update Gitlab Kubernetes Agent in Namespace {ns.Metadata.Name}");

    string output;

    using (var p = new Process())
    {
        p.StartInfo.FileName = Environment.GetEnvironmentVariable("HELM_PATH") ?? "/usr/local/bin/helm";
        p.StartInfo.ArgumentList.Add("-n");
        p.StartInfo.ArgumentList.Add(ns.Metadata.Name);
        p.StartInfo.ArgumentList.Add("list");
        p.StartInfo.ArgumentList.Add("-o");
        p.StartInfo.ArgumentList.Add("json");
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.RedirectStandardOutput = true;
        p.Start();
        output = p.StandardOutput.ReadToEnd();
    }

    var helm_charts = JsonSerializer.Deserialize<List<HelmList>>(output);
    var gitlabAgentChart = helm_charts?.Where(c => c.Name == "gitlab-agent").FirstOrDefault() ?? null;
    if (gitlabAgentChart != null)
    {
        if (gitlabAgentChart.ChartVersion != "gitlab-agent-" + Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION") || Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION") == "latest")
        {
            Console.WriteLine($"Start updating the Gitlab Kubernetes agent found - Chart Version: {gitlabAgentChart.ChartVersion}, last updated: {gitlabAgentChart.Updated}, app version: {gitlabAgentChart.AppVersion} to chart version {Environment.GetEnvironmentVariable("HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION")}");

            using (var p = new Process())
            {// Gitlab Token aus Values auslesen - ggf. direkt aus dem Secret
                p.StartInfo.FileName = Environment.GetEnvironmentVariable("HELM_PATH") ?? "/usr/local/bin/helm";
                p.StartInfo.ArgumentList.Add("-n");
                p.StartInfo.ArgumentList.Add(ns.Metadata.Name);
                p.StartInfo.ArgumentList.Add("get");
                p.StartInfo.ArgumentList.Add("values");
                p.StartInfo.ArgumentList.Add("gitlab-agent");
                p.StartInfo.ArgumentList.Add("-o");
                p.StartInfo.ArgumentList.Add("json");
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                output = p.StandardOutput.ReadToEnd();
            }

            HelmGitlabKubernetesAgentValues? agentValues = JsonSerializer.Deserialize<HelmGitlabKubernetesAgentValues>(output);

            if (agentValues != null)
            {
                Org.OpenAPITools.Controllers.GitlabApiController.InstallGitlabKubernetesAgent(ns.Metadata.Name, agentValues.Config.Token, null);
                Console.WriteLine($"Finished Agent Update in namespace - {ns.Metadata.Name}");
            }
            else
            {
                Console.WriteLine($"ERROR: Could not find Gitlab Kubernetes Agent Token found in {output}");
            }
        }
        else
        {
            Console.WriteLine($"Do nothing because the Gitlab Kubernetes agent is already up to date - Chart Version: {gitlabAgentChart.ChartVersion}, last updated: {gitlabAgentChart.Updated}, app version: {gitlabAgentChart.AppVersion}");
        }
    }
    else
    {
        Console.WriteLine($"ERROR: No Gitlab Kubernetes Agent found in {ns.Metadata.Name}");
    }
}