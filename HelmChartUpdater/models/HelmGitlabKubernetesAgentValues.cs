﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HelmChartUpdater.models
{
    /// <summary>
    /// Model for helm get values <gitlab-agent> JSON output
    /// </summary>
    public class HelmGitlabKubernetesAgentValues
    {
        [JsonPropertyName("config")]
        public HelmGitlabKubernetesAgentValuesConfig Config { get; set; }

        public class HelmGitlabKubernetesAgentValuesConfig
        {
            [JsonPropertyName("token")]
            public string Token { get; set; }
        }
    }
}
