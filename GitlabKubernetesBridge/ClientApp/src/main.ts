import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

export function getBrandString() {
  return window.document.getElementById('branding')?.innerText ?? "";
}

export const providers = [
  { provide: 'BRAND_STRING', useFactory: getBrandString, deps: [] }
];

if (environment.production) {
  enableProdMode();
}

window.document.addEventListener('DOMContentLoaded', () => {
     platformBrowserDynamic(providers).bootstrapModule(AppModule)
  .catch(err => console.log(err));
   });
