﻿namespace GitlabKubernetesBridge.Server.Models.GitlabAPI
{
    public class KubenertesUserConfig
    {
        private readonly string userAccountName;
        private readonly string token;

        public KubenertesUserConfig(string userAccountName, string token)
        {
            this.userAccountName = userAccountName;
            this.token = token;
        }

        /// <summary>
        /// String representation of ~/.kube/config file for specified user
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return @"apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: " + Base64Encode(System.IO.File.ReadAllText("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")) + @"
    server: " + Environment.GetEnvironmentVariable("KUBE_APIURL") + @"
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: " + this.userAccountName + @"
  name: " + this.userAccountName + @"@kubernetes
current-context: " + this.userAccountName + @"@kubernetes
kind: Config
preferences: {}
users:
- name: " + this.userAccountName + @"
  user:
    token: " + this.token;
        }

        static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
